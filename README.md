ejemplo
==========

---

## Intro

Pequeño proyecto symfony 3 para implementación de un API para una lista de la compra.
Cada usuario podrá gestionar sus listas: crear, modificar, borrar, ver elementos, añadir elemento, marcar elemento, eliminar elemento...

---

## Despliegue

1. git clone git@bitbucket.org:vortindev/example.git
2. cd example
3. composer install
4. php app/console doctrine:database:create
5. php app/console doctrine:schema:update --force
6. php app/console doctrine:fixtures:load
7. php app/console server:run

---

## Ejemplos de uso

[Ejemplos de llamadas con Postman](https://documenter.getpostman.com/view/4190296/RW1Vr2ib)