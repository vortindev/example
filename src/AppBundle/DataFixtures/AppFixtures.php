<?php

namespace AppBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Lista;
use AppBundle\Entity\Elemento;

class AppFixtures extends Fixture {

    public function load(ObjectManager $manager) {

        for ($userIndex = 0; $userIndex < 5; $userIndex++) {
            $user = new Usuario();
            $user->setUsername("user$userIndex");
            $user->setPassword(password_hash("user$userIndex", PASSWORD_DEFAULT));
            $manager->persist($user);

            for ($listIndex = 0; $listIndex < 5; $listIndex++) {
                $list = new Lista();
                $list->setName("lista_$userIndex-$listIndex");
                $list->setDescription("description_$userIndex-$listIndex");
                $list->setUsuario($user);
                $manager->persist($list);

                for ($elementIndex = 0; $elementIndex < 10; $elementIndex++) {
                    $element = new Elemento();
                    $element->setName("elemento_$userIndex-$listIndex-$elementIndex");
                    $element->setDescription("description_$userIndex-$listIndex-$elementIndex");
                    $element->setPrice(rand(5, 20));
                    $element->setLista($list);
                    $manager->persist($element);
                }
            }
        }

        $manager->flush();
    }

}
