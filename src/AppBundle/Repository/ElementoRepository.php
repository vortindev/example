<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Lista;

/**
 * ElementoRepository
 *
 */
class ElementoRepository extends \Doctrine\ORM\EntityRepository {
    
    public function findArrayByList(Lista $list) {
        $dql = ''
                . 'SELECT e.id, e.name, e.description, e.done, e.price '
                . 'FROM AppBundle:Elemento e '
                . 'WHERE e.lista = :lista';

        $query = $this->getEntityManager()
                    ->createQuery($dql)
                    ->setParameter('lista', $list);
        
        return $query->getArrayResult();
    }
    
}
