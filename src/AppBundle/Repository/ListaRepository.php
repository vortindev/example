<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Usuario;

/**
 * ListaRepository

 */
class ListaRepository extends \Doctrine\ORM\EntityRepository {

    public function findArrayByUser(Usuario $usuario) {
        $dql = ''
                . 'SELECT l.id, l.name, l.description '
                . 'FROM AppBundle:Lista l '
                . 'WHERE l.usuario = :usuario';

        $query = $this->getEntityManager()
                    ->createQuery($dql)
                    ->setParameter('usuario', $usuario);
        
        return $query->getArrayResult();
    }

}
