<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use AppBundle\Manager\ListManager;
use AppBundle\Entity\Elemento;

/**
 * Description of ElementManager
 *
 * @author vortin
 */
class ElementManager {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface 
     */
    private $tokenInterface;
    
    /**
     * @var ListManager
     */
    private $listManager;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, ListManager $listManager) {
        $this->em = $em;
        $this->tokenInterface = $tokenStorage;
        $this->listManager = $listManager;
    }

    public function getElements($idList) {
        $list = $this->listManager->find($idList);
        return $this->em->getRepository('AppBundle:Elemento')->findArrayByList($list);
    }

    public function find($idList, $id) {
        $list = $this->listManager->find($idList);
        $element = $this->em->getRepository('AppBundle:Elemento')->find($id);

        if (!($element instanceof Elemento)) {
            throw new NotFoundHttpException('Element not found');
        } elseif ($element->getLista() != $list) {
            throw new AccessDeniedException('Access denied');
        }

        return $element;
    }
    
    public function create($idList, $data) {
        $list = $this->listManager->find($idList);
        
        if (empty($data['name']) || empty($data['description']) || empty($data['price'])) {
            throw new BadRequestHttpException('Missing mandatory parameters');
        }
        
        $element = new Elemento();
        $element->setName($data['name']);
        $element->setDescription($data['description']);
        $element->setPrice($data['price']);
        $element->setLista($list);
        
        $this->em->persist($element);
        $this->em->flush();
        
        return $element->getId();
    }
    
    public function update($idList, $id, $data) {
        if (empty($data['name']) || empty($data['description']) || empty($data['price']) || empty($data['done'])) {
            throw new BadRequestHttpException('Missing mandatory parameters');
        }
        
        $list = $this->listManager->find($idList);
        $element = $this->em->getRepository('AppBundle:Elemento')->find($id);

        if (!($element instanceof Elemento)) {
            throw new NotFoundHttpException('Element not found');
        } elseif ($element->getLista() != $list) {
            throw new AccessDeniedException('Access denied');
        }
        
        $element->setName($data['name']);
        $element->setDescription($data['description']);
        $element->setPrice($data['price']);
        $element->setDone($data['done']);
        
        $this->em->persist($element);
        $this->em->flush();
        
        return;
    }
    
    public function remove($idList, $id) {
        $list = $this->listManager->find($idList);
        $element = $this->em->getRepository('AppBundle:Elemento')->find($id);

        if (!($element instanceof Elemento)) {
            throw new NotFoundHttpException('Element not found');
        } elseif ($element->getLista() != $list) {
            throw new AccessDeniedException('Access denied');
        }
        
        $this->em->remove($element);
        $this->em->flush();
        
        return;
    }

}
