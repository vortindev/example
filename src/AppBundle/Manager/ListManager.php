<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use AppBundle\Entity\Lista;

/**
 * Description of ListManager
 *
 * @author vortin
 */
class ListManager {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface 
     */
    private $tokenInterface;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage) {
        $this->em = $em;
        $this->tokenInterface = $tokenStorage;
    }

    public function getLists() {
        $user = $this->getUser();
        return $this->em->getRepository('AppBundle:Lista')->findArrayByUser($user);
    }
    
    public function find($id) {
        $list = $this->em->getRepository('AppBundle:Lista')->find($id);

        if (!($list instanceof Lista)) {
            throw new NotFoundHttpException('List not found');
        } elseif ($list->getUsuario() != $this->getUser()) {
            throw new AccessDeniedException('Access denied');
        }

        return $list;
    }

    public function get($id) {
        $list = $this->find($id);
        $arrayList = json_decode(json_encode($list), true);
        $arrayList['elements'] = $this->em->getRepository('AppBundle:Elemento')->findArrayByList($list);
        
        return $arrayList;
    }
    
    public function create($data) {
        if (empty($data['name']) || empty($data['description'])) {
            throw new BadRequestHttpException('Missing mandatory parameters');
        }
        
        $list = new Lista();
        $list->setName($data['name']);
        $list->setDescription($data['description']);
        $list->setUsuario($this->getUser());
        
        $this->em->persist($list);
        $this->em->flush();
        
        return $list->getId();
    }
    
    public function update($id, $data) {
        if (empty($data['name']) || empty($data['description'])) {
            throw new BadRequestHttpException('Missing mandatory parameters');
        }
        
        $list = $this->em->getRepository('AppBundle:Lista')->find($id);

        if (!($list instanceof Lista)) {
            throw new NotFoundHttpException('List not found');
        } elseif ($list->getUsuario() != $this->getUser()) {
            throw new AccessDeniedException('Access denied');
        }
        
        $list->setName($data['name']);
        $list->setDescription($data['description']);
        
        $this->em->persist($list);
        $this->em->flush();
        
        return;
    }
    
    public function remove($id) {
        $list = $this->em->getRepository('AppBundle:Lista')->find($id);

        if (!($list instanceof Lista)) {
            throw new NotFoundHttpException('List not found');
        } elseif ($list->getUsuario() != $this->getUser()) {
            throw new AccessDeniedException('Access denied');
        }
        
        $this->em->remove($list);
        $this->em->flush();
        
        return;
    }
    
    private function getUser() {
        $user = $this->tokenInterface->getToken() ? $this->tokenInterface->getToken()->getUser() : null;
        if (!$user) {
            throw new AccessDeniedException('Access denied');
        }
        return $user;
    }

}
