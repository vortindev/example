<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

class ExceptionListener {

    public function onKernelException(GetResponseForExceptionEvent $event) {
        
        $exception = $event->getException();
        $code = 400;
        
        if ($exception instanceof NotFoundHttpException) {
            $code = 404;
        } elseif ($exception instanceof AccessDeniedException || $exception instanceof AccessDeniedHttpException) {
            $code = 403;
        }
        
        $response = new JsonResponse([
            'message' => $exception->getMessage()
        ], $code);
        $event->setResponse($response);
    }

}
