<?php

namespace AppBundle\Entity;

/**
 * Elemento
 */
class Elemento implements \JsonSerializable {

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $price;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \AppBundle\Entity\Lista
     */
    private $lista;

    public function __construct() {
        $this->price = 0;
        $this->done = false;
        $this->created = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Elemento
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Elemento
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Elemento
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return Elemento
     */
    public function setDone($done) {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean
     */
    public function getDone() {
        return $this->done;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Elemento
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set lista
     *
     * @param \AppBundle\Entity\Lista $lista
     *
     * @return Elemento
     */
    public function setLista(\AppBundle\Entity\Lista $lista = null) {
        $this->lista = $lista;

        return $this;
    }

    /**
     * Get lista
     *
     * @return \AppBundle\Entity\Lista
     */
    public function getLista() {
        return $this->lista;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'price' => $this->price,
            'done' => $this->done,
        ];
    }

}
