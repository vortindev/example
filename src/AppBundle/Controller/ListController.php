<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Manager\ListManager;

/**
 * Description of ListController
 *
 * @author vortin
 */
class ListController extends Controller {
    
    public function listAction(ListManager $listManager) {
        $lists = $listManager->getLists();
        return $this->json($lists);
    }
    
    public function detailAction(ListManager $listManager, $id) {
        $list = $listManager->get($id);
        return $this->json($list);
    }
    
    public function createAction(Request $request, ListManager $listManager) {
        $data = json_decode($request->getContent(), true);
        $id = $listManager->create($data);
        return $this->json([
            'id' => $id
        ]);
    }
    
    public function updateAction(Request $request, ListManager $listManager, $id) {
        $data = json_decode($request->getContent(), true);
        $listManager->update($id, $data);
        return $this->json([]);
    }
    
    public function removeAction(ListManager $listManager, $id) {
        $listManager->remove($id);
        return $this->json([]);
    }
    
}
