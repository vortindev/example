<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Manager\ElementManager;

/**
 * Description of ElementController
 *
 * @author vortin
 */
class ElementController extends Controller {
    
    public function listAction(ElementManager $elementManager, $idLista) {
        $elements = $elementManager->getElements($idLista);
        return $this->json($elements);
    }
    
    public function detailAction(ElementManager $elementManager, $idLista, $id) {
        $element = $elementManager->get($idLista, $id);
        return $this->json($element);
    }
    
    public function createAction(Request $request, ElementManager $elementManager, $idLista) {
        $data = json_decode($request->getContent(), true);
        $id = $elementManager->create($idLista, $data);
        return $this->json([
            'id' => $id
        ]);
    }
    
    public function updateAction(Request $request, ElementManager $elementManager, $idLista, $id) {
        $data = json_decode($request->getContent(), true);
        $elementManager->update($idLista, $id, $data);
        return $this->json([]);
    }
    
    public function removeAction(ElementManager $elementManager, $idLista, $id) {
        $elementManager->remove($idLista, $id);
        return $this->json([]);
    }
    
}
